package id.bps.akmal.surveymanagement.surveydesign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurveyDesignApplication {

	public static void main(String[] args) {
		SpringApplication.run(SurveyDesignApplication.class, args);
	}
}
